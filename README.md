# Action-Climbing-Wiki

## Description
This Wiki aims to provide collective information on the topic of **Action-Climbing**. While focusing on safety-relevant information most of the time, the goal is also to provide some Guides on how to solve common problems occuring during actions. This repository is the source for https://wiki.actionclimbing.org, which is rendered using https://mkdocs.org.

## TL;DR for Editors
- Everything you want to **publish** to https://wiki.actionclimbing.org has to be inside the "Climbing-Wiki" Folder, otherwise it will just be available to this git repository.
- If you want to upload **images** it is best practice to go to https://img.tedomum.net and insert the markdown link here.
- Try to **quote** and **link** rather than deleting things, whenever possible and useful.

## (No) Moderation
There is no plan to have any moderation team since this project is mainly maintained by a network of people.

**As a general guideline:** If there are several ways to deal with some problem then we aim for listing all of them side by side together with their pros and cons in this wiki. We don't believe in any solution being "the proper solution". 

**Wrong Facts and safety-relevant information:**
However, we encourage everyone to moderate things which are proven wrong - which in a **good practice** would include:
- to **comment** on things which are considered unsafe
- while making sure to point out **why** 
- quoting the exact problematic statement using **blockquotes** (the > sign)
- Adding **Sources** through links is also a good thing whenever possible. Consider [uploading images](https://img.tedomum.net) too.

**Discussions:**
Of-Course, this is not a room to have discussions. So we want to make sure that these stay in chat rooms, mailing lists and the analog world and we reach some common conclusion in this Wiki.

**Reasoning for this:**
We believe that it helps people more to read wrong information which is being corrected, than to straight up delete wrong information. There are so many sayings out there which someone has heard from someone else and they tend to stay in movements for quite a while. So let's make sure everyone has access to information and ongoing discussions to be able to develop their own educated safety-standard, instead of cutting things out trying to have a "one fits all" standard.

## Contact
If you want to reach out to some of us, you can write a mail to climbingnetwork@systemli.org. Otherwise we encourage you to ask around if you know someone involved.

## Contributing
If you want to contribute it is **required for you to know someone who is already involved.** Since Action-Climbing in Europe is done by a fairly small bubble of people, chances are high that you already know someone who knows where to ask for this, if you have been to a Forest Occupation or Action Climbing Training.

## Getting Started
### Obsidian Setup
#### Setup Climbing-Wiki in a Vault
**Please reproduce these steps exactly to add the Climbing-Wiki to your Obsidian App**
1. Open or Install Obsidian on your Device
2. Create a New Vault and Name it e.g. "Climbing-Wiki"
3. Open Settings in that Vault
4. Open Community Plugins and Enable them if not done yet
5. Look for "Obsidian Git" and install it and enable it.
6. Go back to your Vault, Open the Command Palette and type "Git: Clone [...]" and choose that command.
7. Copy & Paste this URL hit enter: https://0xacab.org/ClimbingNetwork/climbing-wiki.git
9. Click on "Vault Root" (when it asks for the .git folder)
10. Click on "YES" (when it asks for the .obsidian folder)
11. Press enter
12. Restart Obsidian

You now have succesfully cloned the Wiki into your Vault and are able to read and edit all the files locally. 

#### Publish your Changes
To be able to sync your changes to GitLab/the Wiki Website, you need to proceed with these steps:

1. Open Settings in this vault
2. Go to Obsidian Git at the Bottom
3. Scroll down to the field **"hostname"** and enter a pseudonym you like:
![Hostname field saying "you"](https://img.tedomum.net/data/Screenshot_20240118-150411-eb6f45.png)
**Note:** *You should put some form of identifier that enables us to figure out who made a change in case we have to revert it. This will be public information, so it shouldnt be your real name and its smart to use a name you've never used before. Please tell your contact which name you have chosen.*
4. Scroll down to the Block **"Authentication"** and enter the following credentials:
![Credentials needed for Sync with git](https://img.tedomum.net/data/Screenshot_20240118-145044-b9eacd.png)
**Note:** *You will still have to ask your contact for the password or access token, so this will not work without you knowing someone already involved.*

## License
This Wiki is public information licensed with GNU-General Public License 3.0. However, we don't really care about that - just use this stuff in terms of copyleft or CC-BY-SA and don't be a capitalist asshole.

## Project status
This project started in January 2024 and is actively maintained.